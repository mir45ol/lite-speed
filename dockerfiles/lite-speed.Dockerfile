FROM node:14-buster

# Expose the API ports.
EXPOSE 80 443

# Change to the Node user.
USER node

# Change to the Lite Speed directory.
WORKDIR /opt/lite-speed

# Run Bash.
ENTRYPOINT ["/bin/bash"]
