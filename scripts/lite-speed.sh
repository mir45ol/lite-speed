#!/bin/bash

# Constants --------------------------------------------------------------------
DOCKER_FILE=`realpath ./dockerfiles/lite-speed.Dockerfile`
DOCKER_IMAGE_NAME='lite-speed'
DOCKER_IMAGE_TAG='0.0.0'

# Utility Functions ------------------------------------------------------------

# Usage: print message
print() {
  printf "$1\n"
}

# Usage: warning warning_message
warning() {
  printf "\e[33m$1\e[m\n"
}

# Usage: error error_message
error() {
  printf "\e[31m$1\e[m\n"
}

# Main Functions ---------------------------------------------------------------

help() {
  print "Usage: `basename $0` [options]"
  print "  -h Display help."
  print "  -b Build a $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG image."
  print "  -i Install npm dependencies."
  print "  -d Rollback all Knex migrations."
  print "  -u Run all Knex migrations."
  print "  -s Run all Knex seeds."
  print "  -t Run a test server."
  print "  -f Run a development server."
  print "  -p Run a production server."
}

build() {
  local DOCKER_RESPONSE=`docker image inspect "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" 2> /dev/null`
  local DOCKER_ERROR_CODE=$?
  if [[ "$DOCKER_ERROR_CODE" == "0" ]]; then
    warning "Found an existing $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG image..."
    warning "Delete image (y/n)?"

    read DELETE_DOCKER_IMAGE
    if [[ "$DELETE_DOCKER_IMAGE" == "y" || "$DELETE_DOCKER_IMAGE" == "Y" ]]; then
      docker rmi "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG"
    fi
  fi

  print "Building a new $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG image..."
  docker build --file="$DOCKER_FILE" --tag="$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" `realpath ./.dockercontext`
}

npmInstall() {
  docker run \
    --volume `realpath ./`:/opt/lite-speed \
    --tty \
    --interactive \
    --rm \
    "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" \
    -c "npm install"
}

knexMigrateRollback() {
  docker run \
    --env-file=`realpath ./envs/postgres.env` \
    --volume `realpath ./`:/opt/lite-speed \
    --tty \
    --interactive \
    --rm \
    "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" \
    -c "npm run knex:migrate:rollback"
}

knexMigrateLatest() {
  docker run \
    --env-file=`realpath ./envs/postgres.env` \
    --volume `realpath ./`:/opt/lite-speed \
    --tty \
    --interactive \
    --rm \
    "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" \
    -c "npm run knex:migrate:latest"
}

knexSeedRun() {
  docker run \
    --env-file=`realpath ./envs/postgres.env` \
    --volume `realpath ./`:/opt/lite-speed \
    --tty \
    --interactive \
    --rm \
    "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" \
    -c "npm run knex:seed:run"
}

testServer() {
  docker run \
    --env-file=`realpath ./envs/test.env` \
    --env-file=`realpath ./envs/postgres.env` \
    --volume `realpath ./`:/opt/lite-speed \
    --publish 80:80 \
    --tty \
    --interactive \
    --rm \
    "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" \
    -c "npm run testServer"
}

devServer() {
  docker run \
    --env-file=`realpath ./envs/development.env` \
    --env-file=`realpath ./envs/postgres.env` \
    --volume `realpath ./`:/opt/lite-speed \
    --publish 80:80 \
    --tty \
    --interactive \
    --rm \
    "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" \
    -c "npm run devServer"
}

prodServer() {
  docker run \
    --env-file=`realpath ./envs/production.env` \
    --env-file=`realpath ./envs/postgres.env` \
    --volume `realpath ./`:/opt/lite-speed \
    --publish 80:80 \
    --publish 443:443 \
    --tty \
    --interactive \
    --rm \
    "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" \
    -c "npm run server:run"
}

while getopts 'hbidustfp' opt; do
  case "$opt" in
    h)
      help
      ;;
    b)
      build
      ;;
    i)
      npmInstall
      ;;
    d)
      knexMigrateRollback
      ;;
    u)
      knexMigrateLatest
      ;;
    s)
      knexSeedRun
      ;;
    t)
      testServer
      ;;
    f)
      devServer
      ;;
    p)
      prodServer
      ;;
    *)
      help
      exit 128
      ;;
  esac
done

if [[ -z "$1" ]]; then
  help
  exit 128
fi
