#!/bin/bash

# Constants --------------------------------------------------------------------
DOCKER_CONTAINER_NAME=lite-speed-postgres

# Utility Functions ------------------------------------------------------------

# Usage: print message
print() {
  printf "$1\n"
}

# Usage: warning warning_message
warning() {
  printf "\e[33m$1\e[m\n"
}

# Usage: error error_message
error() {
  printf "\e[31m$1\e[m\n"
}

# Main Functions ---------------------------------------------------------------

help() {
  print "Usage: `basename $0` [options]"
  print "  -h Display help."
  print "  -s Stop a running $DOCKER_CONTAINER_NAME container."
  print "  -r Run a $DOCKER_CONTAINER_NAME container."
  print "  -a Attach to a running $DOCKER_CONTAINER_NAME container."
}

stop() {
  docker stop "$DOCKER_CONTAINER_NAME"
}

run() {
  docker run \
    --env-file=`realpath ./envs/postgres.env` \
    --volume `realpath ./postgresql-data`:/var/lib/postgresql/data \
    --publish 5432:5432 \
    --tty \
    --interactive \
    --detach \
    --rm \
    --name $DOCKER_CONTAINER_NAME \
    postgres:13
}

attach() {
  docker attach "$DOCKER_CONTAINER_NAME"
}

while getopts 'hsra' opt; do
  case "$opt" in
    h)
      help
      ;;
    s)
      stop
      ;;
    r)
      stop
      run
      ;;
    a)
      attach
      ;;
    *)
      help
      exit 128
      ;;
  esac
done

if [[ -z "$1" ]]; then
  help
  exit 128
fi
