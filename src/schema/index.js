/** @file Load the GraphQL schema and resolvers. */

const { addResolversToSchema } = require('@graphql-tools/schema')
const schema = require('./schema')
const resolvers = require('../resolvers')

const schemaWithResolvers = addResolversToSchema({
  schema,
  resolvers
})

module.exports = schemaWithResolvers
