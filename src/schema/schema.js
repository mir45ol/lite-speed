/** @file Load the GraphQL schema. */

const { join } = require('path')
const { loadSchemaSync } = require('@graphql-tools/load')
const { GraphQLFileLoader } = require('@graphql-tools/graphql-file-loader')

const schemaFiles = join(__dirname, './*.graphql')
const schemaLoaders = [new GraphQLFileLoader()]
const schema = loadSchemaSync(
  schemaFiles,
  { loaders: schemaLoaders }
)

module.exports = schema
