/**
 * Get a hash of args a [Resolver]{@link Resolver} was called with.
 * @param {Object.<string, string>} columns - A Resolver's columns.
 * @param {Object.<string, *>} args - A hash of args a Resolver was called with.
 * @returns {Object.<string, *>} A hash of args a Resolver was called with.
 */
function getArgs(columns, args) {
  return Object.entries(args)
    .reduce(
      (newArgs, [argKey, argValue]) => {
        if (columns[argKey]) {
          Object.assign(
            newArgs,
            { [columns[argKey]]: argValue }
          )
        }

        return newArgs
      },
      {}
    )
}

module.exports = getArgs
