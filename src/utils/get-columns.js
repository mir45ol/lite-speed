/**
 * Get a hash of columns a [Resolver]{@link Resolver} was called with.
 * @param {Object.<string, string>} columns - A Resolver's columns.
 * @param {ResolverField[]} requestedFields - A list of fields a Resolver was called with.
 * @returns {Object.<string, string>} A hash of columns a Resolver was called with.
 */
function getColumns(columns, requestedFields) {
  return requestedFields
    .reduce(
      (newColumns, { name }) => {
        if (columns[name]) {
          Object.assign(
            newColumns,
            { [name]: columns[name] }
          )
        }

        return newColumns
      },
      {}
    )
}

module.exports = getColumns
