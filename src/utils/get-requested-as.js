/**
 * Get a field a [Resolver]{@link Resolver} was called as.
 * @param {string} typeName - A Resolver's type name.
 * @param {*} info - GraphQL info.
 * @returns {ResolverField} A field a Resolver was called as.
 */
function getRequestedAs(typeName, info) {
  const list = /^\[[_A-Za-z][_0-9A-Za-z]*\]!?$/.test(info.returnType.toString())
  const nonNull = info.returnType.toString().endsWith('!')

  return {
    type: typeName,
    list,
    nonNull,
    name: info.fieldName
  }
}

module.exports = getRequestedAs
