const resolvers = require('../resolvers/resolvers')

/**
 * Convert a [Resolver]{@link Resolver} to a GraphQL resolver.
 * @param {Resolver} resolver - A Resolver.
 * @returns {*} A GraphQL resolver.
 */
function toGraphQLResolver(resolver) {
  const nestedGraphQLResolver = Object.values(resolver.fields)
    .filter(field => !field.primitive)
    .reduce(
      (acc, field) => {
        acc[field.name] = resolvers[field.type]._fetch
        return acc
      },
      {}
    )

  const graphQLResolver = {
    _typeName: resolver.typeName,
    Query: { [resolver.fieldName]: resolver._fetch },
    Mutation: { [resolver.fieldName]: () => ({
      create: resolver._create,
      update: resolver._update,
      delete: resolver._delete
    }) },
    [resolver.typeName]: nestedGraphQLResolver
  }

  return graphQLResolver
}

module.exports = toGraphQLResolver
