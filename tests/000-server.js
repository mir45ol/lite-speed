const chai = require('chai')
const chaiHTTP = require('chai-http')

const packageJSON = require('../package')
const config = require('../src/config')

chai.use(chaiHTTP)

describe('Server should be running.', () => {
  it('Info path should return info.', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .get(config.infoPath)
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.name).to.equal(packageJSON.name)
        chai.expect(res.body.version).to.equal(packageJSON.version)
      }))
})
