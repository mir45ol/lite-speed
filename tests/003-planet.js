const chai = require('chai')
const chaiHTTP = require('chai-http')

const config = require('../src/config')

chai.use(chaiHTTP)

const planetsQuery = `
  query(
    $ids: [ID],
    $starIds: [ID]
  ) {
    planet(
      ids: $ids,
      starIds: $starIds
    ) {
      id
      starId
      name
    }
  }
`

const starsWithPlanetsQuery = `
  query($ids: [ID]) {
    star(ids: $ids) {
      id
      name

      planets {
        id
        starId
        name
      }
    }
  }
`

const satellitesWithPlanetsQuery = `
  query($ids: [ID]) {
    satellite(ids: $ids) {
      id
      planetId
      name

      planet {
        id
        starId
        name
      }
    }
  }
`

describe('Planet resolver should work.', () => {
  it('Planet resolver should return all planets.', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({ query: planetsQuery })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.planet).to.have.deep.ordered.members([
          {
            "id": "0",
            "starId": "0",
            "name": "Mercury"
          },
          {
            "id": "1",
            "starId": "0",
            "name": "Venus"
          },
          {
            "id": "2",
            "starId": "0",
            "name": "Earth"
          },
          {
            "id": "3",
            "starId": "0",
            "name": "Mars"
          },
          {
            "id": "4",
            "starId": "0",
            "name": "Jupiter"
          },
          {
            "id": "5",
            "starId": "0",
            "name": "Saturn"
          },
          {
            "id": "6",
            "starId": "0",
            "name": "Uranus"
          },
          {
            "id": "7",
            "starId": "0",
            "name": "Neptune"
          },
          {
            "id": "8",
            "starId": "1",
            "name": "Proxima Centauri B"
          },
          {
            "id": "9",
            "starId": "1",
            "name": "Proxima Centauri C"
          }
        ])
      }))

  it('Planet resolver should return Earth.', () =>
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({
        query: planetsQuery,
        variables: { ids: [2] }
      })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.planet).to.have.deep.ordered.members([
         {
            "id": "2",
            "starId": "0",
            "name": "Earth"
          }
        ])
      }))

  it("Planet resolver should return Sol's planets.", () =>
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({
        query: planetsQuery,
        variables: { starIds: [0] }
      })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.planet).to.have.deep.ordered.members([
          {
            "id": "0",
            "starId": "0",
            "name": "Mercury"
          },
          {
            "id": "1",
            "starId": "0",
            "name": "Venus"
          },
          {
            "id": "2",
            "starId": "0",
            "name": "Earth"
          },
          {
            "id": "3",
            "starId": "0",
            "name": "Mars"
          },
          {
            "id": "4",
            "starId": "0",
            "name": "Jupiter"
          },
          {
            "id": "5",
            "starId": "0",
            "name": "Saturn"
          },
          {
            "id": "6",
            "starId": "0",
            "name": "Uranus"
          },
          {
            "id": "7",
            "starId": "0",
            "name": "Neptune"
          }
        ])
      }))

  it('Star resolver should return all stars with planets.', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({ query: starsWithPlanetsQuery })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.star).to.have.deep.ordered.members([
          {
            "id": "0",
            "name": "Sol",
            "planets": [
              {
                "id": "0",
                "starId": "0",
                "name": "Mercury"
              },
              {
                "id": "1",
                "starId": "0",
                "name": "Venus"
              },
              {
                "id": "2",
                "starId": "0",
                "name": "Earth"
              },
              {
                "id": "3",
                "starId": "0",
                "name": "Mars"
              },
              {
                "id": "4",
                "starId": "0",
                "name": "Jupiter"
              },
              {
                "id": "5",
                "starId": "0",
                "name": "Saturn"
              },
              {
                "id": "6",
                "starId": "0",
                "name": "Uranus"
              },
              {
                "id": "7",
                "starId": "0",
                "name": "Neptune"
              }
            ]
          },
          {
            "id": "1",
            "name": "Proxima Centauri",
            "planets": [
              {
                "id": "8",
                "starId": "1",
                "name": "Proxima Centauri B"
              },
              {
                "id": "9",
                "starId": "1",
                "name": "Proxima Centauri C"
              }
            ]
          }
        ])
      }))

  it('Satellite resolver should return all satellites with planets.', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({ query: satellitesWithPlanetsQuery })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.satellite).to.have.deep.ordered.members([
          {
            "id": "0",
            "planetId": "2",
            "name": "Moon",
            "planet": {
              "id": "2",
              "starId": "0",
              "name": "Earth"
            }
          },
          {
            "id": "1",
            "planetId": "3",
            "name": "Phobos",
            "planet": {
              "id": "3",
              "starId": "0",
              "name": "Mars"
            }
          },
          {
            "id": "2",
            "planetId": "3",
            "name": "Deimos",
            "planet": {
              "id": "3",
              "starId": "0",
              "name": "Mars"
            }
          }
        ])
      }))
})
